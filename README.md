# wacai

#### 介绍

一个简单的web记账项目,使用了SpringBoot和java

#### 功能

| 分支 | 增加功能 |
| ---- | ---- |
| v1   | 一个简单的index页面,没有前后端交互|
| v2   | 增加了accounting页面，能够让用户输入收入和支出，输出余额|
| v3   | 增加了面向对象，构建了一个用户的对象，来计算余额|
| v4   | 加上了ArrayList存储消费记录     |
| v5   | 加上StreamFilter来过滤记录，支持查询大于某个消费金额的记录    |
| v6   | 加入了分期支付功能，根据用户输入的数据，返回分期支付的对象     |
| v7   | 加入了理财页面，使用接口实现，可以输入理财金额和选择理财产品，计算收益   |
| v8   | 加入文件存储，能够将记录储存在excel文件上，并且能将文件上储存的记录再转化成对象，然后输出|

#### 遇到的问题和收获

- 遇到了文件读写的问题，文件读取之后一直不能改文件名，最后才发现是做读写操作的时候，没有关闭流导致的。
- 在这次项目，我使用了spring boot 框架，和Apache POI库,实现了一个前端Web页面、后端Java交互的网页。学习到了spring boot的一些知识，学会了使用库来读写excel文件
- 改进的地方：这次代码我写的注释很少，在调试遇到bug的时候就很难受，多写点注释。